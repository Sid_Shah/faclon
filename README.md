# faclon

Hi,
Please refer these steps before proceeding.

clone the package 
run npm install
run node server.js or (sudo) node server.js if required.

node is running on port 80.

API's with sample data is given below
You can run them in postman.

1. localhost/api/prime/:number
enter any number and it will give you the prime numbers till that given number
example: localhost/api/prime/4
will return 2 and 3.

2. localhost/api/users
you need to post data in the body by referring this example.
{
"name" : "Siddhant shah",
"email" : "shah.siddhant.it@gmail.com",
"password": "123456",
"address" : "201 ankit apartment",
"phone" : "8109332043",
"birth" : "11-08-1997"
}
change the values.
email and phone has to be unique.

3. localhost/api/contact
post data in the body by referring this example.
{
"contact" : "checking",
"email" : "shah.siddhant.it@gmail.com"
}
it gets added only if the email matches in the user document.
once created the reference to contact document gets added to the contacts array in the user document.

4. localhost/api/deleteUser
post data in the body by referring this example.
{
"email" : "shah.siddhant.it@gmail.com"
}
the user document gets deleted and all the contacts associated with also gets deleted.

Please revert back if any queries or changes.

Thanks,
Siddhant Shah.

