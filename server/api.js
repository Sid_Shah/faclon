const {
    check,
    validationResult
} = require('express-validator/check');
const create = require('../functions/create');
module.exports = (router) => {

    // prime Numbers
    router.get('/prime/:key', async (req, res) => {

        const key = Number.parseInt(req.params.key); 
        if (isNaN(key)) {
            res.send('please enter a number');
        } else {

            try {
                const Prime = await getPrimeNumbers(key); // calling getPrimeNumbers function
                res.send(Prime);
            } catch (err) {
                res.send(err);
            }
        }

    });

    // to add user
    router.post('/users', [check('email').isEmail(), check('password').isLength({
            min: 6
        })],
        async (req, res) => {
            try {
                const errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return res.status(500).json({
                        errors: errors.array()
                    });
                }
                const user = await create.registerUser(req.body);
                res.send(user);
            } catch (err) {
                res.send(err);
            }
        })

        // route to add contacts
    router.post('/contact', [check('email').isEmail()], async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(500).json({
                    errors: errors.array()
                });
            }
            const contact = await create.addContact(req.body);
            res.send(contact);
        } catch (err) {
            res.send(err);
        }
    })

    // deleting user and its dependent contacts
    router.post('/deleteUser', [check('email').isEmail()], async (req, res) => {
        try {
            const deletedUser = await create.deleteUser(req.body); // call to deleteUser function
            res.send(deletedUser);
        } catch (err) {
            res.send(err);
        }
    })
// to find all prime numbers
function getPrimeNumbers (limit) {

    const allNumbers = [];
    const primes = [];
    let i;
    let l;
    allNumbers[1] = false;
    for (i = 2; i <= limit; i += 1) {
      allNumbers[i] = true;
    }
    for (i = 2; i * i <= limit; i += 1) {
      if (allNumbers[i] !== true) {
        continue;
      }
      for (l = i * i; l <= limit; l += i) {
        allNumbers[l] = false;
      }
    }
    allNumbers.forEach(function (value, key) {
      if (value) {
        primes.push(key);
      }
    });
    return primes;
  };
}