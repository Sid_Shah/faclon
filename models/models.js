'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// user Schema
const userSchema = Schema({
    name: String,
    email: {
        type: String,
        trim: true,
        lowercase: true,
        required: [true, "email is required"]
    },
    password: String,
    address: String,
    phone: {
        type: Number,
        required: [true, "phone no is required"]
    },
    birth: Date,
    contacts: {
        type: Array
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

// compound index
userSchema.index({email : 1,contact : 1},{unique : true})

// contact schema
const contactsSchema = Schema({
    contact: String,
    email: String
});

// pre save contacts check if the email id exists in the user collection
contactsSchema.pre('save', function (next) {
    user.countDocuments({
        email: this.email
    }).then((data) => {
        if (data) {
            return next();
        } else {
            return next(new Error('no user for the contact'));
        }
    }, (err) => {

        return next(new Error('error'));
    });
})


// post save contacts push the contacts reference to the user contacts array
contactsSchema.post('save', function (next) {
    user.find({
        email: this.email
    }).then((data) => {
        const singleUser = data[0];
        singleUser.contacts.push(this._id);
        return singleUser.save();

    })
})

// pre remove user delete associated contacts
userSchema.pre('remove', {
    query: true,
    doc: true
}, async function (next) {
    const cont = await contact.deleteMany({
        email: this._conditions.email
    });
    next();
})


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://Siddhant:Coffee12@ds029821.mlab.com:29821/faclon');

const user = mongoose.model('user', userSchema);
const contact = mongoose.model('contacts', contactsSchema);
module.exports = {
    user,
    contact
};