const bcrypt = require('bcryptjs');
const models = require('../models/models');
const user = models.user;
const contacts = models.contact;
// registering user
exports.registerUser = async (body) => {
    const salt = bcrypt.genSaltSync(10); 
      const hash = bcrypt.hashSync(body.password, salt); // hashing password
      body.password = hash;
    try {
      const newUser = new user(body);
      await newUser.save(); // saving new document
        
      return Promise.resolve({
        status: 200,
        message: 'User Registered Sucessfully !'
      })
  
    } catch (err) {
      
      if (err.code == 11000) { // duplicate error
  
        return {
          status: 409,
          message: err
        };
  
      } else {
  
        return {
          status: 500,
          message: err
        };
      }
    };
  
  
  };

// add contact
exports.addContact = async (body) => {
    try {
    const contact = new contacts({   
        contact : body.contact,
        email :body.email
       });
  await contact.save(); // saving contact
    return contact;
} catch (err) {
    return 'cannot find email';
}
}

// delete user
exports.deleteUser = async (body) => {
try {
   await user.remove(body); // removing user
    return user;
} catch(err) {
    return 'cannot find email';
}
}
