const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const http = require('http');
const router = express.Router();
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));


const server = http.createServer(app);
server.listen(80);


const api = require('./server/api');


require('./server/api')(router);
console.log("server running!");


app.use('/api', router);